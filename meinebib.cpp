#include<stdio.h>

int Mittelwert(int summe, int teiler);
int MittelwertxZahlen();
int Fakultaet(int n);
int* SortArray(int array[3]);

int Mittelwert(int summe, int teiler)
{
	int mittelwert = summe / teiler;
	printf("Der Mittelwert der %i eingegebenen Zahlen betr\x84gt: %i.\n", teiler, mittelwert);
}

int MittelwertxZahlen()
{
	printf("Bitte gib mir ein paar Zahlen!\n");
	int zahl, summe = 0, counter = 1;
	do
	{
		// Zahl einlesen
		printf("Zahl %i: ", counter);
		scanf_s("%i", &zahl);
		summe += zahl;
		//printf("Zahl = %i, counter = %i\n", zahl, counter); //Kontrolle
		counter += 1;
	} while (zahl != 0);
	Mittelwert(summe, counter - 1);
}

int Fakultaet(int n)
{
	////Zahl eingeben
	//int n;
	//printf("Ganze Zahl eingeben bitte: ");
	//scanf_s("%i", &n);
	int f = 1;
	for (int i = 1; i < n; i++)
	{
		f = f * (i + 1);
		//printf("%i! = %i\n",n, f);
	}
	printf("Die Fakult\x84t von %i betr\x84gt %i!\n", n, f);
}

int* SortArray(int array[3]) {
	//static int a[5];
	//int arraysize = 3;
	int arraysize = *(&array + 1) - array;
	int runde = 0;
	int tempVal = 0;
	for (int j = 0; j < arraysize; j++)
	{
		for (int k = j + 1; k < arraysize; k++)
		{
			if (array[j] > array[k])
			{
				tempVal = array[k];
				array[k] = array[j];
				array[j] = tempVal;
			}
		}
		runde++;
	}

	for (int i = 0; i < 5; i++)
	{
		printf("Array-Wert %i: ", i + 1);
		scanf_s("%i", &array[i]);
	}
	return (array);
}